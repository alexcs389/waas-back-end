require('dotenv').config()

const express = require('express');
const bodyParser = require('body-parser');

const userRouter = require('./routes/user');
const userInformationRouter = require('./routes/userInformation');

const PORT = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('');
});

app.use('/api/v1/users', userRouter);
app.use('/api/v1/user-information', userInformationRouter);

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});

