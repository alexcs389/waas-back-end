const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

dotenv.config();

exports.generateAccessToken = function (data) {
    return jwt.sign(data, process.env.TOKEN_SECRET, { expiresIn: '86400s' });
}

exports.authenticateToken = function (req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.TOKEN_SECRET, (err, data) => {
        if (err) {
            console.log(err);
            return res.sendStatus(403);
        }

        req.data = data;
        next();
    });
}
