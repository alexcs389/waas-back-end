const express = require('express');
const router  = express.Router();
const userInformationController = require('../controllers/userInformation');
const jsonWebToken = require('../services/json-web-token');

router.get('/', jsonWebToken.authenticateToken, userInformationController.index);
router.post('/', jsonWebToken.authenticateToken, userInformationController.create);
router.get('/:id', jsonWebToken.authenticateToken, userInformationController.show);
router.put('/:id', jsonWebToken.authenticateToken, userInformationController.update);
router.delete('/:id', jsonWebToken.authenticateToken, userInformationController.destroy);

module.exports = router;
