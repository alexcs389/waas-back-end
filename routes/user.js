const express = require('express');
const router  = express.Router();
const userController = require('../controllers/user');
const jsonWebToken = require('../services/json-web-token');

router.get('/', jsonWebToken.authenticateToken, userController.index);
router.post('/', jsonWebToken.authenticateToken, userController.create);
router.get('/:id', jsonWebToken.authenticateToken, userController.show);
router.put('/:id', jsonWebToken.authenticateToken, userController.update);
router.delete('/:id', jsonWebToken.authenticateToken, userController.destroy);

module.exports = router;
