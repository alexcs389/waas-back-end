const models = require('../models');
const User = models.User;
const jsonWebToken = require('../services/json-web-token');

exports.index = function (req, res) {
    console.log(req.data);
    res.status(200).json({message: 'INDEX USERS', users: User.findAll()});
};

exports.create = function (req, res) {
    console.log(req);
    const token = jsonWebToken.generateAccessToken({message: 'Hello World!'});
    res.status(201).json({message: 'CREATE USERS', token: token});
};

exports.show = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'SHOW USERS'});
};

exports.update = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'UPDATE USERS'});
};

exports.destroy = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'DELETE USERS'});
};
