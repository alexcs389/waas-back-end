const models = require('../models');
const UserInformation = models.UserInformation;

exports.index = function (req, res) {
    res.status(200).json({message: 'INDEX USER INFORMATION', user_information: UserInformation.findAll()});
};

exports.create = function (req, res) {
    console.log(req.params);
    res.status(201).json({message: 'CREATE USER INFORMATION'});
};

exports.show = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'SHOW USER INFORMATION'});
};

exports.update = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'UPDATE USER INFORMATION'});
};

exports.destroy = function (req, res) {
    console.log(req.params);
    res.status(200).json({message: 'DELETE USER INFORMATION'});
};
