'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Publication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Publication.belongsTo(models.User, {as: 'user'});
      Publication.hasOne(models.TypePublication, {as: 'typePublication'});
      Publication.hasMany(models.Like, {as: 'likes'});
      Publication.hasMany(models.Commentary, {as: 'commentaries'});
    }
  };
  Publication.init({
    typePublicationId: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Publication',
  });
  return Publication;
};
