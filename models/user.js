'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.TypeUser, {as: 'typeUser'});
      User.belongsToMany(models.Language, {as: 'languages', through: 'UsersLanguages'});
      User.belongsToMany(models.Speciality, {as: 'specialities', through: 'UserSpecialities'});
      User.hasOne(models.UserInformation, {as: 'userInformation'});
      User.hasOne(models.CompanyInformation, {as: 'companyInformation'});
      User.hasMany(models.Like, {as: 'likes'});
      User.hasMany(models.Publication, {as: 'publications'});
      User.hasMany(models.Commentary, {as: 'commentaries'});
      User.hasMany(models.Schedule, {as: 'schedules'});
      User.hasMany(models.Directory, {as: 'directories'});
      User.hasMany(models.Scholarship, {as: 'scholarships'});
    }
  };
  User.init({
    typeUserId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
