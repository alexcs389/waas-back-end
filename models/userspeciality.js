'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserSpeciality extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserSpeciality.belongsTo(models.User, {as: 'user'});
      UserSpeciality.belongsTo(models.Speciality, {as: 'speciality'});
    }
  };
  UserSpeciality.init({
    userId: DataTypes.INTEGER,
    specialityId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserSpeciality',
  });
  return UserSpeciality;
};
