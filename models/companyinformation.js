'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CompanyInformation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      CompanyInformation.belongsTo(models.User, {as: 'user'});
    }
  };
  CompanyInformation.init({
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CompanyInformation',
  });
  return CompanyInformation;
};
