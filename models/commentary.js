'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Commentary extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Commentary.belongsTo(models.User, {as: 'user'});
      Commentary.belongsTo(models.Publication, {as: 'publication'});
    }
  };
  Commentary.init({
    userId: DataTypes.INTEGER,
    publicationId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Commentary',
  });
  return Commentary;
};
