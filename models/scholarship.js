'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Scholarship extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Scholarship.belongsTo(models.User, {as: 'user'});
      Scholarship.belongsTo(models.TypeSchooling, {as: 'typeSchooling'});
    }
  };
  Scholarship.init({
    userId: DataTypes.INTEGER,
    typeSchoolingId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Scholarship',
  });
  return Scholarship;
};
