'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UsersLanguage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UsersLanguage.belongsTo(models.User, {as: 'user'});
      UsersLanguage.belongsTo(models.Language, {as: 'language'});
    }
  };
  UsersLanguage.init({
    userId: DataTypes.INTEGER,
    languageId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UsersLanguage',
  });
  return UsersLanguage;
};
