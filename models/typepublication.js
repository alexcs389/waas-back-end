'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TypePublication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      TypePublication.hasMany(models.Publication, {as: 'publications'});
    }
  };
  TypePublication.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'TypePublication',
  });
  return TypePublication;
};
